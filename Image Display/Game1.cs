﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System;
using System.Collections;
using System.IO;

namespace Image_Display
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        
        public GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteBatch overlay;

        // Game state stuff
        const int STATE_SPLASH = 0;
        const int STATE_GAME = 1;
        const int STATE_GAMEOVER = 2;

        bool fullScreen = false;

        int gameState = STATE_SPLASH;

        int score = 0;
        int hiScore = 0;

        SpriteFont impactFont;
        Texture2D rectang;
        Color[] color;

        Color rainbow;
        Vector2 scorePoint;

        Song spaceMus;

        public Texture2D shipTexture;
        Texture2D asteroidTexture;
        Texture2D bulletTexture;
        Texture2D titleScreen;
        Texture2D gameOver;

        Player p;

        public float deltaTime;
        public float currentSpeed = 0;
        public float playerSpeed = 15;
        public float playerRotateSpeed = 5;
        public Vector2 playerPosition = new Vector2(0, 0);
        public Vector2 playerOffset = new Vector2(0, 0);
        public float playerAngle = 0;
        public bool playerAlive = true;

        float asteroidSpeed = 40;
        Vector2 asteroidOffset = new Vector2(0, 0);
        ArrayList asteroidPositions = new ArrayList();
        ArrayList asteroidVelocities = new ArrayList();
        ArrayList asteroidRotations = new ArrayList();
        Vector2 prevDir = new Vector2(0,0);


        public float bulletSpeed = 2000;
        public Vector2 bulletPosition = new Vector2(0, 0);
        public Vector2 bulletVelocity = new Vector2(0, 0);
        public bool bulletAlive = false;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            this.IsFixedTimeStep = false;
            this.graphics.SynchronizeWithVerticalRetrace = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            playerPosition = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2, graphics.GraphicsDevice.Viewport.Height / 2);

            p = new Player();

            hiScore = LoadHiScore();

            scorePoint = new Vector2(-666, -666);

            rectang = new Texture2D(graphics.GraphicsDevice, 170, 60);
            color = new Color[170 * 60];
            for (int i = 0; i < color.Length; i++) color[i] = Color.Black;
            rectang.SetData(color);

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            overlay = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            impactFont = Content.Load<SpriteFont>("Impact");
            shipTexture = Content.Load<Texture2D>("ship");
            asteroidTexture = Content.Load<Texture2D>("asteroid2");
            bulletTexture = Content.Load<Texture2D>("bullet");
            titleScreen = Content.Load<Texture2D>("Title");
            gameOver = Content.Load<Texture2D>("gameover");

            // Calculate offset
            playerOffset = new Vector2(shipTexture.Width / 2, shipTexture.Height / 2);
            asteroidOffset = new Vector2(asteroidTexture.Width / 2, asteroidTexture.Height / 2);

            // Play dat music
            spaceMus = Content.Load<Song>("music");
            MediaPlayer.Play(spaceMus);
            MediaPlayer.IsRepeating = true;
            MediaPlayer.MediaStateChanged += MediaPlayer_MediaStateChanged;

            // Spawn Asteroids
            SpawnAsteroids();
            
        }

        void MediaPlayer_MediaStateChanged(object sender, System.EventArgs e)
        {
            MediaPlayer.Volume -= 0.1f;
            MediaPlayer.Play(spaceMus);
        }

        private void SpawnAsteroids()
        {
            Random random = new Random();
            if (asteroidPositions.Count < 10)
            {
                Vector2 randDirection = new Vector2(
                    random.Next(-100, 100), random.Next(-100, 100));
                randDirection.Normalize();

                if (randDirection == prevDir)
                    return;

                float rotate = (float)random.Next(-1, 2);
                asteroidRotations.Add(rotate);
                prevDir = randDirection;

                Vector2 asteroidPosition = randDirection * 2 * graphics.GraphicsDevice.Viewport.Height;
                asteroidPositions.Add(asteroidPosition);

                Vector2 velocity = (playerPosition - asteroidPosition);
                velocity.Normalize();
                velocity *= asteroidSpeed;

                

                asteroidVelocities.Add(velocity);
            }
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
                

            if (Keyboard.GetState().IsKeyDown(Keys.F) && !fullScreen)
            {
                graphics.IsFullScreen = !graphics.IsFullScreen;
                graphics.ApplyChanges();
            }

            fullScreen = Keyboard.GetState().IsKeyDown(Keys.F);

            // TODO: Add your update logic here
            deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            if (score > hiScore)
                hiScore = score;

            switch (gameState)
            {
                case STATE_SPLASH:
                    UpdateSplashState();
                    break;
                case STATE_GAME:
                    UpdateGameState();
                    break;
                case STATE_GAMEOVER:
                    UpdateGameOverState();
                    break;
            }

            Random rand = new Random();
            if (gameTime.TotalGameTime.Milliseconds % 50 == 0)
                rainbow = new Color(rand.Next(0, (int)asteroidSpeed), rand.Next(0, (int)asteroidSpeed), rand.Next(0, (int)asteroidSpeed));

            base.Update(gameTime);
        }

        private void UpdateSplashState()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                score = 0;
                gameState = STATE_GAME;
            }
                
            
        }
        private void DrawSplashState(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(titleScreen, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
        }

        private void UpdateGameState()
        {
            UpdatePlayer();
            UpdateAsteroids(deltaTime);
            UpdateBullets(deltaTime);

            // Hitboxes for players & bullets
            Rectangle bulletRect = new Rectangle(
                (int)bulletPosition.X,
                (int)bulletPosition.Y,
                bulletTexture.Bounds.Width,
                bulletTexture.Bounds.Height);

            Rectangle playerRect = new Rectangle(
                (int)(playerPosition.X - playerOffset.X),
                (int)(playerPosition.Y - playerOffset.Y),
                shipTexture.Bounds.Width,
                shipTexture.Bounds.Height);

            // check for collisions
            for (int asteroidIdx = 0; asteroidIdx < asteroidPositions.Count; asteroidIdx++)
            {
                Vector2 position = (Vector2)asteroidPositions[asteroidIdx];
                Rectangle asteroid = new Rectangle(
                    (int)(position.X - asteroidOffset.X),
                    (int)(position.Y - asteroidOffset.Y),
                    asteroidTexture.Width, asteroidTexture.Height);

                if (IsColliding(bulletRect, asteroid) && bulletAlive)
                {
                    scorePoint = (Vector2)asteroidPositions[asteroidIdx];
                    bulletAlive = false;
                    asteroidPositions.RemoveAt(asteroidIdx);
                    asteroidVelocities.RemoveAt(asteroidIdx);
                    asteroidRotations.RemoveAt(asteroidIdx);
                    score += Convert.ToInt32(asteroidSpeed);
                    asteroidSpeed += 5;
                    break;
                }
                if (IsColliding(asteroid, playerRect))
                {
                    gameState = STATE_GAMEOVER;
                    scorePoint = new Vector2(-666, -666);
                    asteroidSpeed = 60;
                    SaveHiScore(hiScore);
                }
                    

                bool isAtEdge(Vector2 pos)
                {
                    if (pos.X < asteroidTexture.Width || pos.X > GraphicsDevice.Viewport.Width - asteroidTexture.Width || pos.Y < asteroidTexture.Height || pos.Y > GraphicsDevice.Viewport.Height - asteroidTexture.Height)
                        return true;
                    else
                        return false;
                }

                for (int otherIdx = 0; otherIdx < asteroidPositions.Count && otherIdx != asteroidIdx; otherIdx++)
                {
                    Vector2 otherPos = (Vector2)asteroidPositions[otherIdx];
                    Rectangle otherRect = new Rectangle(
                    (int)(otherPos.X - asteroidOffset.X),
                    (int)(otherPos.Y - asteroidOffset.Y),
                    asteroidTexture.Width, asteroidTexture.Height);

                    if (IsColliding(asteroid, otherRect) && !isAtEdge(position) && !isAtEdge(otherPos))
                    {
                        asteroidVelocities[asteroidIdx] = -(Vector2)asteroidVelocities[asteroidIdx];
                        asteroidVelocities[otherIdx] = -(Vector2)asteroidVelocities[otherIdx];
                    }
                }
            }

            if (scorePoint.X < 10)
                scorePoint.X = 10;
            if (scorePoint.X > GraphicsDevice.Viewport.Width)
                scorePoint.X = GraphicsDevice.Viewport.Width - 60;
            if (scorePoint.Y < 30)
                scorePoint.Y = 30;
            if (scorePoint.Y > GraphicsDevice.Viewport.Height - 30)
                scorePoint.Y = GraphicsDevice.Viewport.Height - 30;


        }

        private void DrawGameState(SpriteBatch spriteBatch)
        {
            if (playerAlive)
                spriteBatch.Draw(shipTexture, playerPosition, null, null, playerOffset, playerAngle, null, Color.White);

            for (int asteroidIdx = 0; asteroidIdx < asteroidPositions.Count; asteroidIdx++)
            {
                Vector2 position = (Vector2)asteroidPositions[asteroidIdx];
                spriteBatch.Draw(asteroidTexture, position, null, null, asteroidOffset, (float)asteroidRotations[asteroidIdx], null, new Color(255 - rainbow.R, 255 - rainbow.B, 255 - rainbow.G));
                spriteBatch.Draw(bulletTexture, new Vector2(GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height) - position, Color.White);
            }

            if (bulletAlive)
                spriteBatch.Draw(bulletTexture, bulletPosition, null, null, null, 0, null, Color.White);

            

        }

        private void UpdateGameOverState()
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                gameState = STATE_SPLASH;
                asteroidPositions = new ArrayList();
                asteroidVelocities = new ArrayList();
                playerPosition = new Vector2(graphics.GraphicsDevice.Viewport.Width / 2, graphics.GraphicsDevice.Viewport.Height / 2);
                playerAngle = 0;
                score = 0;
            }
                
        }
        private void DrawGameOverState(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(gameOver, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            

            switch (gameState)
            {
                case STATE_SPLASH:
                    DrawSplashState(spriteBatch);
                    break;
                case STATE_GAME:
                    DrawGameState(spriteBatch);
                    break;
                case STATE_GAMEOVER:
                    DrawGameOverState(spriteBatch);
                    break;
            }

            spriteBatch.Draw(rectang, new Vector2(0, 0), Color.Black);

            spriteBatch.End();

            overlay.Begin();

            overlay.DrawString(impactFont, "      Score: " + score.ToString("000000"), new Vector2(10, 5), Color.White);

            overlay.DrawString(impactFont, "Hi Score: " + hiScore.ToString("000000"), new Vector2(10, 30), Color.White);

            overlay.DrawString(impactFont, "+" + asteroidSpeed, scorePoint, rainbow);

            overlay.End();


            base.Draw(gameTime);
        }

        private void UpdatePlayer()
        {
            if (!playerAlive)
                return;

            p.UpdatePlayer(this);
        }

        private void UpdateAsteroids(float deltaTime)
        {
            for (int asteroidIdx = 0; asteroidIdx < asteroidPositions.Count; asteroidIdx++)
            {
                Vector2 position = (Vector2)asteroidPositions[asteroidIdx];
                Vector2 velocity = (Vector2)asteroidVelocities[asteroidIdx];

                position += velocity * deltaTime;
                asteroidPositions[asteroidIdx] = position;

                if (position.X < -asteroidTexture.Width / 2)
                    position.X = graphics.GraphicsDevice.Viewport.Width + asteroidTexture.Width / 2;
                if (position.X > graphics.GraphicsDevice.Viewport.Width + asteroidTexture.Width / 2)
                    position.X = -asteroidTexture.Width / 2;
                if (position.Y < -asteroidTexture.Height / 2)
                    position.Y = graphics.GraphicsDevice.Viewport.Height + asteroidTexture.Height / 2;
                if (position.Y > graphics.GraphicsDevice.Viewport.Height + asteroidTexture.Height / 2)
                    position.Y = -asteroidTexture.Width / 2;

                asteroidRotations[asteroidIdx] = (float)asteroidRotations[asteroidIdx] + 0.000001f * asteroidSpeed;

                asteroidPositions[asteroidIdx] = position;
            }
            SpawnAsteroids();

        }

        private void UpdateBullets(float deltaTime)
        {
            if (!bulletAlive)
                return;

            bulletPosition += bulletVelocity * deltaTime;

            if (bulletPosition.X < 0 ||
                bulletPosition.X > graphics.GraphicsDevice.Viewport.Width ||
                bulletPosition.Y < 0 ||
                bulletPosition.Y > graphics.GraphicsDevice.Viewport.Height)
            {
                bulletAlive = false;
            }
        }

        private bool IsCollidingCircle(Vector2 position1, float radius1, Vector2 position2, float radius2)
        {
            Vector2 distance = position2 - position1;

            if (distance.Length() < radius1 + radius2)
            {
                return true;
            }

            return false;
        }

        private bool IsColliding(Rectangle rect1, Rectangle rect2)
        {
            if (rect1.X + rect1.Width < rect2.X ||
                rect1.X > rect2.X + rect2.Width ||
                rect1.Y + rect1.Height < rect2.Y ||
                rect1.Y > rect2.Y + rect2.Height)
            {
                return false;
            }

            return true;
        }
        void SaveHiScore(int score)
        {
            string userName = System.Environment.UserName;

            // Code Score
            byte[] codedScore = System.Text.Encoding.UTF8.GetBytes(score.ToString());
            string returnValue = System.Convert.ToBase64String(codedScore);

            System.IO.File.WriteAllText(@"C:\Users\" + userName + @"\Desktop\Highscore.txt", returnValue);
        }
        int LoadHiScore()
        {
            string userName = System.Environment.UserName;

            string path = @"C:\Users\" + userName + @"\Desktop\Highscore.txt";

            if (!File.Exists(path))
                return 0;

            StreamReader sr = new StreamReader(path);

            string codedData = sr.ReadLine();

            byte[] encodedData = System.Convert.FromBase64String(codedData);

            return Convert.ToInt32(System.Text.Encoding.UTF8.GetString(encodedData));
        }
    }
}
