﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Image_Display
{
    class Player
    {
        Game1 main;

        int recoil = 0;

        public void UpdatePlayer(Game1 main)
        {
            if (Keyboard.GetState().IsKeyDown(Keys.Up) == true)
            {
                if (main.currentSpeed < 0.03)
                    main.currentSpeed += main.playerSpeed * main.deltaTime;
            }
            else if (main.currentSpeed > 0)
            {
                main.currentSpeed -= 0.00001f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down) == true)
            {
                if (main.currentSpeed > -0.02)
                    main.currentSpeed += -main.playerSpeed * main.deltaTime;
            }
            else if (main.currentSpeed < 0)
            {
                main.currentSpeed += 0.00001f;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left) == true)
            {
                main.playerAngle -= main.playerRotateSpeed * main.deltaTime;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Right) == true)
            {
                main.playerAngle += main.playerRotateSpeed * main.deltaTime;
            }

            // Do angle stuff
            Vector2 playerDirection = new Vector2(-(float)Math.Sin(main.playerAngle), (float)Math.Cos(main.playerAngle));

            playerDirection.Normalize();


            // Shooting

            if (recoil > 0)
                recoil--;

            if (Keyboard.GetState().IsKeyDown(Keys.Space) == true && main.bulletAlive == false && recoil == 0)
            {
                recoil = 1000;
                main.bulletPosition = main.playerPosition;
                main.bulletVelocity = playerDirection * main.bulletSpeed;
                main.bulletAlive = true;
                if (!Keyboard.GetState().IsKeyDown(Keys.Down))
                    main.currentSpeed -= 0.01f;
            }


            Vector2 direction = new Vector2(40, 30);
            direction.Normalize();

            Vector2 playerVelocity = playerDirection * main.currentSpeed;
            main.playerPosition += playerVelocity;

            // Screen Wrap
            if (main.playerPosition.X < -main.shipTexture.Width / 2)
                main.playerPosition.X = main.graphics.GraphicsDevice.Viewport.Width + main.shipTexture.Width / 2;
            if (main.playerPosition.X > main.graphics.GraphicsDevice.Viewport.Width + main.shipTexture.Width / 2)
                main.playerPosition.X = -main.shipTexture.Width / 2;
            if (main.playerPosition.Y < -main.shipTexture.Height / 2)
                main.playerPosition.Y = main.graphics.GraphicsDevice.Viewport.Height + main.shipTexture.Height / 2;
            if (main.playerPosition.Y > main.graphics.GraphicsDevice.Viewport.Height + main.shipTexture.Height / 2)
                main.playerPosition.Y = -main.shipTexture.Height / 2;
        }
    }
}
